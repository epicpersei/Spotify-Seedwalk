<?php 
class SeedSpotify
{
	function requestCode() {

		$request = "https://accounts.spotify.com/authorize/?client_id=".$this->clientId."&response_type=code&redirect_uri=".urlencode($this->return_uri)."&scope=".urlencode($this->scope)."&state=34fFs29kd09";
		header('Location: '.$request);	
	}
	
	function return64CS() {
		return base64_encode($this->clientId.':'.$this->client_secret);
	}

	function userProfile() {
		$url = "https://api.spotify.com/v1/me";

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$_SESSION['access_token']
			));


		//execute post
		$result = json_decode(curl_exec($ch));
		

		//close connection
		curl_close($ch);

		echo '<pre>';
		echo var_dump($result);
		echo '<pre>';
	}


	function checkToken() {
		if (isset($_SESSION['access_token'])) {

			$this->refreshToken();
		}
	}

	function refreshToken() {
		echo $_SESSION['refresh_token'];
		$fields = array(
			'grant_type' => 'refresh_token',
			'refresh_token' => $_SESSION['refresh_token']
			);

		//url-ify the data for the POST
		$counter = 1;
		foreach($fields as $key=>$value) {
			if ($counter == 1){
				$fields_string = $key.'='.urlencode($value); 
			} else {
				$fields_string = $fields_string.'&'.$key.'='.$value; 
				
			}
			$counter++;
		}

		$url ='https://accounts.spotify.com/api/token';

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Basic '.$this->return64CS()
			));



		//execute post
		$result = json_decode(curl_exec($ch));
		

		//close connection
		curl_close($ch);
		if (property_exists($result, 'access_token') ){
			$this->storeToken($result);
		}
		// echo 'hay refresh';
	

	}


	function requestToken($code) {

		$fields = array(
			'grant_type' => 'authorization_code',
			'code' => $code ,
			'redirect_uri' => $this->return_uri,
			);

		$url ='https://accounts.spotify.com/api/token';

		//url-ify the data for the POST
		$counter = 1;
		foreach($fields as $key=>$value) {
			if ($counter == 1){
				$fields_string = $key.'='.urlencode($value); 
			} else {
				$fields_string = $fields_string.'&'.$key.'='.$value; 
				
			}
			$counter++;
		}
		
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Basic '.$this->return64CS()
			));

		//execute post
		$result = json_decode(curl_exec($ch));
		curl_close($ch);


		//store Token
		$this->storeToken($result);
		return $result;
	}


	function storeToken($json) {
		
		$_SESSION['access_token'] 	= $json->access_token;
		$_SESSION['token_type'] 	= $json->token_type;
		$_SESSION['refresh_token']	= $json->refresh_token;
	}

}


